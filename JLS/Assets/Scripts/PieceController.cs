﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PieceController : MonoBehaviour
{

    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private int count;

    void Start()
    {
        winTextObject.SetActive(false);
    }

    public void IncreaseCount()
    {
        count = count + 1;
        SetCountText();
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 5)
        {
            winTextObject.SetActive(true);
        }
    }
}
