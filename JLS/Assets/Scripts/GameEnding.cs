﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{


    public GameObject loseTextObject;
    public GameObject instructionsGameObject;
    public GameObject johnLemonGameObject;

    public TextMeshProUGUI timerText;

    private float timeLeft = 110.0f;


    void Start()
    {
        loseTextObject.SetActive(false);
        instructionsGameObject.SetActive(true);
    }

    void Update ()
    {
        if (timeLeft < 100.0f)
        {
            instructionsGameObject.SetActive(false);
        }

        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            timerText.text = timeLeft.ToString();
        }
        else
        {
            EndLevel();
            Destroy(johnLemonGameObject);
        }
    }

    void EndLevel ()
    {
        loseTextObject.SetActive(true);
    }
}
