﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Threading;

public class GamePiece : MonoBehaviour
{
    public TextMeshProUGUI countText;
    public PieceController pc;

    private int count;

    void OnMouseDown()
    {
        pc.IncreaseCount();

        Destroy(gameObject);
    }
}
